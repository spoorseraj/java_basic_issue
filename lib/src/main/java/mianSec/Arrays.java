package mianSec;

public class Arrays {
    private static int[][] myArrays = new int[2][7];

    public static void main(String[] args) {
        init();
        for (int[] second : myArrays) {
            for (int item : second) {
                print(item);
            }
        }
        println();
        for (int j = 0; j < myArrays[0].length; j++) {
            for (int i = 0; i < myArrays.length; i++) {
                print(myArrays[i][j]);
            }
        }

    }

    private static void init() {
        myArrays[0] = new int[]{1, 3, 5, 7, 9, 11, 13};
        myArrays[1] = new int[]{2, 4, 6, 8, 10, 12, 14};
    }

    private static void print(Object out) {
        System.out.print(out + " ");
    }

    private static void println() {
        System.out.println(" ");
    }
}
