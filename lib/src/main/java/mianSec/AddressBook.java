package mianSec;

import java.util.List;

public class AddressBook {
    private List<Person> persons;
    private int count;

    public AddressBook(List<Person> persons) {
        this.persons = persons;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
