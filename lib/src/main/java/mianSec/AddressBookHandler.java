package mianSec;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddressBookHandler {
    public static void main(String[] args) {
        Person p1 = new Person("Saedeh", "Poorseraj", "Tehran", "vanak", "2139934", "09121111111");
        Person p2 = new Person("Saeed", "ameri", "Tehran", "sattari", "2133874", "09122222222");
        Person p3 = new Person("Sarah", "Rezaee", "Tehran", "valiasr", "2133874", "09123333333");
        Person p4 = new Person("hasti", "moshtagh", "Tehran", "jordan", "2133874", "09124444444");
        List<Person> persons = new ArrayList<Person>();
        List<Person> sortLastNamePersons = new ArrayList<Person>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);
        AddressBook addressBook = new AddressBook(persons);
        addressBook.setCount(persons.size());
        List<Person> personItems = addressBook.getPersons();
        for (Person p : personItems) {
            print(p.getFirstName() + " " + p.getLastName() + " " + p.getCity() + " " + p.getAddress() + " " + p.getPhone());
        }
        print(addressBook.getCount());
        // ------------------------------------------
        sortLastNamePersons = sort_lastName(persons);
        for (Person person : sortLastNamePersons) {
            print(person.getFirstName() + " " + person.getLastName() + " " + person.getCity() + " " + person.getAddress() + " " + person.getPhone());
        }
    }

    private static void print(Object item) {
        System.out.println(item);
    }

    private static List<Person> sort_lastName(List<Person> persons) {
        List<Person> sortPersons = new ArrayList<Person>();
        List<String> names = new ArrayList<String>();
        for (Person p : persons) {
            names.add(p.getLastName());
        }
        Collections.sort(names, String.CASE_INSENSITIVE_ORDER);
        for (String name : names) {
//            print(name);
            for (Person p : persons) {
                if (name.equals(p.getLastName())) {
                    sortPersons.add(p);
                    break;

                }

            }
        }
        return sortPersons;
    }


}
